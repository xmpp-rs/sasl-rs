Version 0.5.0, released 2021-01-12:
  * Important changes
    - Relicensed to MPL-2.0 from LGPL-3.0-or-later.
    - Made all of the errors into enums, instead of strings.
  * Small changes
    - Replaced rand\_os with getrandom.
    - Bumped all dependencies.

Version 0.4.2, released 2018-05-19:
  * Small changes
    - Marc-Antoine Perennou updated the openssl and base64 dependencies to 0.10.4 and 0.9.0 respectively.
    - lumi updated them further to 0.10.7 and 0.9.1 respectively.
